#!/usr/bin/env python

import getopt, sys, datetime, json, requests

class ParseApiResults():
    
    def usage(self):
            print """
-h --help
-d --debug
--startdate YYYY-MM-DD
--enddate   YYYY-MM-DD
""" 
    def run(self):
        # get cmd line stuff
        self.startdate = False
        self.enddate = False
        lazada_api = 'https://api.hasoffers.com/Apiv3/json?NetworkId=lazada&Target=Affiliate_Report&Method=getConversions&api_key=d6c1ceabfd02cbfeddb61d8dcbb68dac266164598f94c7e7c3e90d9bff667033&fields%5B%5D=Offer.name&fields%5B%5D=Stat.approved_payout&fields%5B%5D=Stat.currency&fields%5B%5D=Stat.sale_amount&groups%5B%5D=Offer.name'
        #&data_start=2015-11-05&data_end=2015-11-05
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                "hd",
                ["help", "debug", "startdate=", "enddate="])
        except getopt.GetoptError, err:
            print str(err)
            sys.exit(2)
        for o, a in opts:
            if o in ("-d", "--debug"):
                self.debug = True
            elif o in ("-h", "--help"):
                self.usage() 
                sys.exit()
            elif o in ("--startdate"):
                self.startdate = a
            elif o in ("--enddate"):
                self.enddate = a
            else:
                assert False, "unhandled option"
                
        if not self.startdate:
            today = datetime.datetime.now()
            self.startdate = today.strftime('%Y-%m-%d')
            self.enddate = self.startdate
            print "no date given, getting report for today - {0}".format(self.startdate)
        else:
            try:
                tempdate = datetime.datetime.strptime(self.startdate, '%Y-%m-%d')
                self.startdate = tempdate.strftime('%Y-%m-%d')
            except Exception, err:
                print "{0} is not a valid YYYY-MM-DD start date".format(self.startdate)
                sys.exit(2)
                
            try:
                tempdate = datetime.datetime.strptime(self.enddate, '%Y-%m-%d')
                self.enddate = tempdate.strftime('%Y-%m-%d')
            except Exception, err:
                print "{0} is not a valid YYYY-MM-DD end date".format(self.enddate)
                sys.exit(2)

        try:
            url = '{0}&data_start={1}&data_end={2}'.format(lazada_api, 
                self.startdate, self.enddate)
            print 'API url: {0}'.format(url)
            
            r = requests.get(url)
            if r.status_code != requests.codes.ok:
                print 'Lazada API failed. response_code={0}. url={1}'.format(
                    r.status_code, url)
                sys.exit(1)
            print 'response code={0}'.format(r.status_code)
            report_json_data = r.text
            
            self.report_results = json.loads(report_json_data)
            
        except Exception, err:
            print str(err)
            sys.exit(2)
            
    
    def main(self):
        outputfile = 'lazada_report_{0}-to-{1}.csv'.format(self.startdate, self.enddate)
        
        if 'response' not in self.report_results and \
            'data' not in self.report_results['response'] and \
            'data' not in self.report_results['response']['data']:
                
            print 'Invalid response from Lazada/HasOffers'
            print '{0}'.format(self.report_results)
            sys.exit(1)
        else:
            print 'there are {0} rows'.format(len(self.report_results['response']['data']['data']))
        
        f = open(outputfile, 'w')
        
        f.write('Offer_name,Offer_id,Stat_approved_payout,Stat_currency,Stat_sale_amount,Stat_sale_amount_local_currency\n')
        
        for row in self.report_results['response']['data']['data']:
            offername = row['Offer']['name']
            offerid = row['Offer']['id']
            stat1 = row['Stat']['approved_payout']
            stat2 = row['Stat']['currency']
            stat3 = row['Stat']['sale_amount']
            key = 'sale_amount@{0}'.format(stat2)
            stat4 = row['Stat'][key]
            output = '{1},{2},{3},{4},{5},{6}\n'.format(offername,offerid,
                stat1,stat2,stat3,stat4,stat4)
            f.write(output)
            
        sys.exit(0)
        

if __name__ == "__main__":
    r = ParseApiResults()
    r.run()
    r.main()          
